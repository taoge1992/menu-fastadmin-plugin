<?php
/*
 * @Descripttion: 用户收货地址验证器
 * @Author: DanceLynx
 * @Date: 2020-11-18 10:10:36
 * @LastEditors: DanceLynx
 * @LastEditTime: 2020-11-18 11:36:04
 */
namespace addons\sdcmenu\validate;

class Address extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'name' => 'require|min:2|max:10',
        'mobile' => 'require',
        'address' => 'require',
        'province_name' => 'require',
        'city_name' => 'require',
        'area_name' => 'require',
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'name.require' => '姓名不能为空',
        'name.min' => '姓名不能小于2位',
        'name.max' => '姓名不能大于10位',
        'mobile.require' => '手机号不能为空',
        'address.require' => '地址不能为空',
        'province_name.require' => '省级名称不能为空',
        'city_name.require' => '市级名称不能为空',
        'area_name.require' => '区县级名称不能为空',
        
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}