<?php
namespace addons\sdcmenu\controller\api;

use addons\sdcmenu\model\Product as ModelProduct;
use think\Validate;

class Product extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $model = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelProduct();
    }

    public function index()
    {
        $where = [];
        // 默认id排序
        $orderKey = 'id';
        if($category = $this->request->request('category')){
            $where['category'] = $category;
        }
        // if($order = $this->request->request('order')){
        //     switch($order){
        //         case 'score':
        //             $orderKey = 'score';
        //         break;
        //         case 'view':
        //             $orderKey = 'view';
        //         break;
        //     }
        // }
        $products = $this->model
            ->where($where)
            ->order($orderKey,'DESC')
            ->field('id,category,desc_images,main_image,title,price',false)
            ->paginate();
        $this->success(__('Request success'),$products);
    }

    
    public function show()
    {
        $id = $this->request->request('id');
        if(! Validate::checkRule($id,'require|number')){
            $this->error(__("Id param must required"));
        }
        $product = $this->model->find($id);
        if(! $product){
            $this->error(__('Not found product'));
        }
        $this->success(__('Request success'),$product);
    }
}