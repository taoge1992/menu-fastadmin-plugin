<?php
namespace addons\sdcmenu\controller\api;

use addons\sdcmenu\model\Category as ModelCategory;
use fast\Tree;

class Category extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $model = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelCategory();
    }

    public function index()
    {
        $type = $this->request->request('type','menu');
        $where = ['type'=>$type,'status'=>'normal'];
        $tree = Tree::instance();
        $tree->init(collection($this->model->where($where)->order('weigh desc,id desc')->select())->toArray(), 'pid');
        $categorylist = $tree->getTreeArray(0);
        $this->success(__('Request Success'),$categorylist);
    }
    
}